package Views.Adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import POJO.Person;
import indyvision.com.myapplication.R;

/**
 * Created by Alexandru on 06-Jul-15.
 */
public class PersonsAdapter extends BaseAdapter {

    /**
     * The context.
     */
    private Context context;
    /**
     * The persons list
     */
    private ArrayList<Person> items;

    /**
     * The layout inflater
     */
    private LayoutInflater layoutInflater;

    /**
     * The constructor for the persons adapter
     * @param context the context
     * @param items the persons list
     */
    public PersonsAdapter(Context context, ArrayList<Person> items) {
        this.layoutInflater = ((Activity)context).getLayoutInflater();
        this.context = context;
        this.items = items;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;

        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.person_layout, parent, false);

            holder = new ViewHolder();

            holder.firstNameTextview = (TextView) convertView.findViewById(R.id.person_layout_first_name_textview);
            holder.lastNameTextview = (TextView) convertView.findViewById(R.id.person_layout_last_name_textview);
            holder.ageTextview = (TextView) convertView.findViewById(R.id.person_layout_age_textview);
            holder.genderTextview = (TextView) convertView.findViewById(R.id.person_layout_gender_textview);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.firstNameTextview.setText(items.get(position).getFirstName());
        holder.lastNameTextview.setText(items.get(position).getLastName());
        holder.ageTextview.setText(items.get(position).getAge() + "");
        holder.genderTextview.setText(items.get(position).getGender());
        holder.ageTextview.setTextColor(0xff0000);

        return convertView;
    }

    public void setNewPersons(ArrayList<Person> newPersons)
    {
        this.items = newPersons;
    }

    private class ViewHolder {
        TextView firstNameTextview;
        TextView lastNameTextview;
        TextView ageTextview;
        TextView genderTextview;
    }
}
