package POJO;

import Utils.Constants;

/**
 * Created by Alexandru on 06-Jul-15.
 */
public class Person {
    /**
     * The person's first name
     */
    private String firstName;

    /**
     * The person's last name
     */
    private String lastName;

    /**
     * The person's age
     */
    private int age;

    /**
     * The person's gender
     */
    private String gender;

    /**
     * Gets the person's first name
     * @return the person's first name
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Sets the person's first name
     * @param firstName the person's first name
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Gets the person's last name
     * @return the person's last name
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Sets the person's last name
     * @param lastName the person's last name
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * Gets the person's age
     * @return the person's age
     */
    public int getAge() {
        return age;
    }

    /**
     * Sets the person's age
     * @param age The person's age. It must be a value higher than 0, and lower than 130.
     * @return true if the age was successfully set, false otherwise
     */
    public boolean setAge(int age) {
        if(age > 0 && age < 130) {
            this.age = age;
            return true;
        }
        return false;
    }

    /**
     * Gets the person's gender
     * @return the person's gender
     */
    public String getGender() {
        return gender;
    }

    /**
     * Sets the person's gender
     * @param gender The person's gender. It must be either "Male" or "Female". Use Constants.male and Constants.female.
     * @return true if the gender was successfully set, false otherwise
     */
    public boolean setGender(String gender) {
        if(gender.equals(Constants.female) || gender.equals(Constants.male)) {
            this.gender = gender;
            return true;
        }
        return false;
    }
}
