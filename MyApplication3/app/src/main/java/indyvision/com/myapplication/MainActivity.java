package indyvision.com.myapplication;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import java.util.ArrayList;

import POJO.Person;
import Utils.Constants;
import Views.Adapters.PersonsAdapter;


public class MainActivity extends ActionBarActivity {

    ListView personsListView;
    PersonsAdapter personsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initLayout();
    }

    private ArrayList<Person> getPersons()
    {
        Person p = new Person();
        p.setFirstName("FirstName 1");
        p.setLastName("LastName 1");
        p.setAge(1);
        p.setGender(Constants.female);
        Constants.personArrayList.add(p);
        Constants.personArrayList.add(p);
        Constants.personArrayList.add(p);
        Constants.personArrayList.add(p);
        Constants.personArrayList.add(p);
        Constants.personArrayList.add(p);
        Constants.personArrayList.add(p);
        Constants.personArrayList.add(p);
        Constants.personArrayList.add(p);
        Constants.personArrayList.add(p);
        Constants.personArrayList.add(p);
        Constants.personArrayList.add(p);
        Constants.personArrayList.add(p);
        Constants.personArrayList.add(p);
        Constants.personArrayList.add(p);
        Constants.personArrayList.add(p);
        Constants.personArrayList.add(p);
        Constants.personArrayList.add(p);
        Constants.personArrayList.add(p);
        Constants.personArrayList.add(p);
        Constants.personArrayList.add(p);
        Person p2 = new Person();
        p2.setFirstName("FirstName 2");
        p2.setLastName("LastName 2");
        p2.setAge(5);
        p2.setGender(Constants.male);
        Constants.personArrayList.add(p2);

        return Constants.personArrayList;
    }

    private void initLayout()
    {
        personsListView = (ListView) findViewById(R.id.activity_main_persons_listview);
        personsAdapter = new PersonsAdapter(MainActivity.this, getPersons());
        personsListView.setAdapter(personsAdapter);
    }

    private void refresh()
    {
        if(personsAdapter != null)
        {
            personsAdapter.setNewPersons(Constants.personArrayList);
            personsAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onResume()
    {
        refresh();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_person_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
